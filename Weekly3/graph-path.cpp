/**********************************************
* File: graph-path.cpp
* Author: Matthew Shan
* Email: mshan@nd.edu
*
* QUESTION 1 - WEEKLY 3
*
* Contains an STL implementation of an Adjacency List
* graph (no directions or weights) using C++ STL Hash
*
* Starter graph code taken from Professor Morrison's inclass example
* Additional inspiration from https://www.geeksforgeeks.org/find-if-there-is-a-path-between-two-vertices-in-a-given-graph/
*
**********************************************/

#include <iostream>
#include <vector>
#include <list>
#include <unordered_map>

// data structure to store graph edges
template<class T>
struct Edge {
	T src, dest;
	int weight;
};

// class to represent a graph object
template<class T>
class Graph {
	public:
		// construct a unordered_map of vectors to represent an adjacency list
		std::unordered_map< T, std::vector< Edge<T> > > adjList;

		// Origin
		T origin;

		/********************************************
		* Function Name  : Graph
		* Pre-conditions : const std::vector< Edge<T> > &edges, int numVerticies
		* Post-conditions: none
		*
		* Graph Constructor
		********************************************/
		Graph(const std::vector< Edge<T> > &edges, int numVerticies) {
			// resize the vector to N elements of type vector<int>
			adjList.rehash(numVerticies);

			// Set the origin
			origin = edges.at(0).src;

			// add edges to the directed graph
			for (auto &edge: edges) {

				// Insert Origin
				addEdge(edge);

				// Since this is undirected, put in opposite
				// Uncomment if graph is directional
				// Create a temp edge to reverse origin and destination
				// Edge<T> tempEdge = {edge.dest, edge.src, edge.weight};
				// addEdge(tempEdge);

			}
		}

		/********************************************
		* Function Name  : addEdge
		* Pre-conditions : Edge<T> edge
		* Post-conditions: none
		*
		********************************************/
		void addEdge(Edge<T> edge){

			// If the word has not yet been hashed
			if(adjList.count(edge.src) == 0){

				// Create a temp vector to store the first destination
				std::vector< Edge<T> > vecTemp;
				vecTemp.push_back(edge);

				// Put the source in, and set
				adjList.insert( {edge.src, vecTemp} );
			} else{
				// Push back on the element
				adjList[edge.src].push_back(edge);
			}

		}

        /********************************************
		* Function Name  : pathExists
		* Pre-conditions : T start, T end
		* Post-conditions: bool
		*
        * Checks to see if a path exists between two edges of a graph
		********************************************/
        bool pathExists(T start, T end) {
            // perform initial check
            if (start == end)
                return true;

            std::unordered_map<T, bool> visitedVerts;
            std::list<T> queue;

            // initialize visited nodes as false
            for (auto it = adjList.begin(); it != adjList.end(); ++it) {
                visitedVerts.insert( { (*it).first, false } );
            }

            visitedVerts[start] = true;
            queue.push_back(start);

            // iterate queue
            while (!queue.empty()) {

                start = queue.front();
                queue.pop_front();

                auto temp = adjList[start];
                // iterate through destination edges
                for (int it = 0; it < temp.size(); it++) {

                    if (temp[it].dest == end)
                        return true;

                    if (!visitedVerts[temp[it].dest]) {
                        visitedVerts[temp[it].dest] = true;
                        queue.push_back(temp[it].dest);
                    }
                }
            }
            // return false if no path found
            return false;
        }
};


/********************************************
* Function Name  : printGraph
* Pre-conditions : const Graph<T>& graph
* Post-conditions: none
*
* Prints all the elements in the graph
********************************************/
template<typename T>
void printGraph(const Graph<T>& graph){

	std::cout << "Number of buckets is: " << graph.adjList.size() << std::endl;
	std::cout << "Origin Node is: " << graph.origin << std::endl;
	std::cout << "Origin: {Destination, Weight}" << std::endl;

	// typename to allow for dependent scope
	for(auto iter = graph.adjList.begin() ; iter != graph.adjList.end(); iter++){

		std::cout << iter->first << ": ";

		for(int i = 0; i < iter->second.size(); i++){
			std::cout << "{" << iter->second[i].dest << ", ";
			std::cout << iter->second[i].weight << "} ";
		}

		std::cout << std::endl;
	}
}




/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main Driver Function
********************************************/
int main(int argc, char** argv)
{

	// Allocate the edges as shown in the description
	std::vector< Edge<char> > edges =
	{
		{'N', 'O', 1}, {'N', 'T', 1}, {'O', 'R', 1},
		{'O', 'E', 1}, {'T', 'R', 1}, {'R', 'D', 1},
		{'E', 'D', 1}, {'Q', 'O', 1}
	};

	// Number of nodes in the graph
	int numVerticies = 7;

	// construct graph
	Graph<char> graph(edges, numVerticies);

	// print adjacency list representation of graph
	printGraph(graph);

    if (graph.pathExists('N', 'R'))
        std::cout << "Path exists between N and R\n";
    else
        std::cout << "Path does not exist between N and R\n";

    if (graph.pathExists('T', 'D'))
        std::cout << "Path exists between T and D\n";
    else
        std::cout << "Path does not exist between T and D\n";

    if (graph.pathExists('Q', 'N'))
        std::cout << "Path exists between Q and N\n";
    else
        std::cout << "Path does not exist between Q and N\n";

	return 0;
}
