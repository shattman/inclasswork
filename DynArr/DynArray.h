/*****************************************
 * Filename: DynArray.h
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 *
 * This file contains the class for
 * a templated dynamic array
 * **************************************/

#ifndef DYNARRAY_H
#define DYNARRAY_H

#include <iostream>

template<class T>
class Array{

    public:

        /******************************
         * Function Name: Array
         * Preconditions: int
         * Postconditions: none
         * This is the constructor for the dynamic array class
         * ****************************/
        Array(const int n = 0) : length(n), capacity(n ? n : 4), data(new T[capacity]) {}

        /******************************
         * Function Name: ~Array
         * Preconditions: none
         * Postconditions: none
         * This is the destructor for the Array class
         * ****************************/
        ~Array() {
            delete [] data;
        }

        /******************************
         * Function Name: operator=
         * Preconditions: Array<T>
         * Postconditions: Array<T>&
         * This is the assignment operator for the array class
         * ****************************/
        Array<T>& operator=(Array<T> other) {
            length = other.length;
            capacity = other.capacity;
            data = new T[capacity];
    	    copy(other.data, other.data + length, data);
    	    return *this;
        }

        /******************************
         * Function Name: Array
         * Preconditions: const Array<T>&
         * Postconditions: none
         * This is the copy constructor for the array class
         * ****************************/
        Array(const Array<T>& other) : length(other.length), capacity(other.capacity), data(new T[capacity]) {
    	    copy(other.data, other.data + length, data);
        }

        /******************************
         * Function Name: swap
         * Preconditions: Array<T>&
         * Postconditions: none
         * This is the copy constructor for the Array Class
         * ****************************/
        void swap(Array<T>& other) {
            swap(length, other.length);
        	swap(capacity, other.capacity);
        	swap(data, other.data);
        }

         /******************************
         * Function Name: push_back
         * Preconditions: const T&
         * Postconditions: none
         *
         * Adds the element to the array. If capacity has been
         * reached, then the array capacity will be extended.
         * ****************************/
        void push_back(const T &value) {
            if (length >= capacity) {
                capacity = capacity * 4 / 3;
                T* tmp = new T[capacity];
                copy(data, data + length, tmp);
                delete [] data;
                data = tmp;
            }
            data[length] = value;
            length++;
        }

        /******************************
         * Function Name: size
         * Preconditions: none
         * Postconditions: int
         * Returns the current size of the array
         * ****************************/
        const int size() const{
            return length;
        }

		const int getCapacity() const{
			return capacity;
		}

        /******************************
         * Function Name: operator[]
         * Preconditions: const int
         * Postconditions: T&
         * This function is the array index operator
         * ****************************/
        T& operator[](const int i){
            return data[i];
        }

        /******************************
         * Function Name: begin
         * Preconditions: none
         * Postconditions: T*
         * Returns a pointer to the beginning of
         * the dynamic array
         * ****************************/
        T* begin(){
            return data;
        }

        /******************************
         * Function Name: end
         * Preconditions: none
         * Postconditions: T*
         * Returns a pointer to the end of the
         * dynamic array
         * ****************************/
        T* end(){
            return data + length;
        }

        /******************************
         * Function Name: operator<<
         * Preconditions: std::ostream &out, Array<T> &array
         * Postconditions: ostream&
         * This function is the stream operator for the
         * dynamic array
         * ****************************/
        friend std::ostream& operator<< (std::ostream &out, Array<T> &array){
            for(int i = 0; i < array.size(); i++){
                out << array[i] << " ";
            }
            return out;
        }

    private:
        int length;
        int capacity;
        T* data;

        /******************************
         * Function Name: swap
         * Preconditions: T2&, T2&
         * Postconditions: none
         *
         * This swaps the members, and is called
         * by the public void swap(Array<T>& other)
         * ****************************/
        template<class T2>
        void swap (T2& a, T2& b)
        {
            T2 c(a);
            a = b;
            b = c;
        }

        /******************************
         * Function Name: copy
         * Preconditions: InputIterator first, InputIterator last, OutputIterator result
         * Postconditions: OutputIterator
         * Copies the elements in the range [first,last) into the range beginning at result.
         * ****************************/
        template<class InputIterator, class OutputIterator>
        OutputIterator copy (InputIterator first, InputIterator last, OutputIterator result)
        {
            while (first!=last) {
                *result = *first;
                ++result; ++first;
            }
            return result;
        }
};

#endif
