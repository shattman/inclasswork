/**********************************************
* File: hash.cpp
* Author: Matthew Shan
* Email: mshan@nd.edu
*
*  Question 2: use nd.txt as text file and search for frequency of a word
**********************************************/

// imports
#include <iostream>
#include <unordered_map>
#include <string>
#include <fstream>
#include <algorithm>

using namespace std;

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char *argv[]
* Post-conditions: int
*
* Main driver for program that takes in file of strings and adds them to
* unordered map and finds frequency of inputed word
********************************************/
int main(int argc, char *argv[]) {

    // set initial unordered_map and strings
    unordered_map<string, int> bookMap;
    string input, search;

    if (argc == 1) {
        // interactive mode
        string filename;
        cout << "Please enter a filename: ";
        cin >> filename;
        ifstream file(filename);
        if (file) {
            while(file >> input) {
                transform(input.begin(), input.end(), input.begin(), ::tolower);
                if (!isalpha(input.back())) {
                    input.pop_back();
                }
                bookMap[input]++;
            }
        } else {
            cout << "Error: invalid file\n";
            return 1;
        }
    } else if (argc == 2) {
        // file in command line arguments
        ifstream file(argv[1]);
        if (file) {
            while(file >> input) {
                transform(input.begin(), input.end(), input.begin(), ::tolower);
                if (!isalpha(input.back())) {
                    input.pop_back();
                }
                bookMap[input]++;
            }
        } else {
            cout << "Error: invalid file\n";
            return 1;
        }
    } else {
        cout << "Error: Too many arguments!";
        return 1;
    }

    // prompt user for search
    cout << "Input word to search for: ";
    cin >> search;
    cout << "Frequency of '" << search << "': " << bookMap[search] << '\n';
}
