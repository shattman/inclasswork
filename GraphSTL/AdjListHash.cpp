/**********************************************
* File: AdjListHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*
* Contains an STL implementation of an Adjacency List
* graph (no directions or weights) using C++ STL Hash
*
* mmorri22@remote305:~/CSE21312/InClass/GraphSTL$ g++ -g -std=c++11 -Wpedantic AdjListHash.cpp -o AdjListHash
* mmorri22@remote305:~/CSE21312/InClass/GraphSTL$ ./AdjListHash
* Number of buckets is: 6
* Origin Node is: N
* Origin: {Destination, Weight}
* E: {O, 1} {D, 1}
* D: {R, 1} {E, 1}
* R: {O, 1} {T, 1} {D, 1}
* T: {N, 1} {R, 1}
* O: {N, 1} {R, 1} {E, 1}
* N: {O, 1} {T, 1}
**********************************************/

#include <iostream>
#include <vector>
#include <unordered_map>

// data structure to store graph edges
template<class T>
struct Edge {
	T src, dest;
	int weight;
};

// class to represent a graph object
template<class T>
class Graph
{
	public:
		// construct a unordered_map of vectors to represent an adjacency list
		std::unordered_map< T, std::vector< Edge<T> > > adjList;

		// Origin
		T origin;

		/********************************************
		* Function Name  : Graph
		* Pre-conditions : const std::vector< Edge<T> > &edges, int numVerticies
		* Post-conditions: none
		*
		* Graph Constructor
		********************************************/
		Graph(const std::vector< Edge<T> > &edges, int numVerticies)
		{
			// resize the vector to N elements of type vector<int>
			adjList.rehash(numVerticies);

			// Set the origin
			origin = edges.at(0).src;

			// add edges to the directed graph
			for (auto &edge: edges)
			{

				// Insert Origin
				addEdge(edge);

				// Since this is undirected, put in opposite
				// Uncomment if graph is directional
				// Create a temp edge to reverse origin and destination
				Edge<T> tempEdge = {edge.dest, edge.src, edge.weight};
				addEdge(tempEdge);

			}
		}

		/********************************************
		* Function Name  : addEdge
		* Pre-conditions : Edge<T> edge
		* Post-conditions: none
		*
		********************************************/
		void addEdge(Edge<T> edge){

				// If the word has not yet been hashed
				if(adjList.count(edge.src) == 0){

					// Create a temp vector to store the first destination
					std::vector< Edge<T> > vecTemp;
					vecTemp.push_back(edge);

					// Put the source in, and set
					adjList.insert( {edge.src, vecTemp} );
				}
				else{
					// Push back on the element
					adjList[edge.src].push_back(edge);
				}

		}
};


/********************************************
* Function Name  : printGraph
* Pre-conditions : const Graph<T>& graph
* Post-conditions: none
*
* Prints all the elements in the graph
********************************************/
template<typename T>
void printGraph(const Graph<T>& graph){

	std::cout << "Number of buckets is: " << graph.adjList.size() << std::endl;
	std::cout << "Origin Node is: " << graph.origin << std::endl;
	std::cout << "Origin: {Destination, Weight}" << std::endl;

	// typename to allow for dependent scope
	for(auto iter = graph.adjList.begin() ; iter != graph.adjList.end(); iter++){

		std::cout << iter->first << ": ";

		for(int i = 0; i < iter->second.size(); i++){
			std::cout << "{" << iter->second[i].dest << ", ";
			std::cout << iter->second[i].weight << "} ";
		}

		std::cout << std::endl;
	}
}


/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main Driver Function
********************************************/
int main(int argc, char** argv)
{

	// Allocate the edges as shown in the description
	std::vector< Edge<char> > edges =
	{
		{'N', 'O', 1}, {'N', 'T', 1}, {'O', 'R', 1},
		{'O', 'E', 1}, {'T', 'R', 1}, {'R', 'D', 1},
		{'E', 'D', 1}
	};

	// Number of nodes in the graph
	int numVerticies = 6;

	// construct graph
	Graph<char> graph(edges, numVerticies);

	// print adjacency list representation of graph
	printGraph(graph);

	return 0;
}
